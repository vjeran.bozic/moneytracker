package com.helloworld.moneytracker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.helloworld.moneytracker.Model.Data;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.text.DateFormat;
import java.util.Date;

public class QR_activity extends AppCompatActivity {

    private EditText input;
    private ImageView display_qr;
    private Button generate, scan;

    private FirebaseAuth mAuth;
    private DatabaseReference mIncomeDatabase;
    private DatabaseReference mExpenseDatabase;

    //don't forget to mentioned camera permission in manifest

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_activity);

        input = (EditText) findViewById(R.id.input);
        display_qr = (ImageView) findViewById(R.id.display_qr);
        generate = (Button) findViewById(R.id.generate);
        scan = (Button) findViewById(R.id.scan);

        mAuth=FirebaseAuth.getInstance();
        FirebaseUser mUser=mAuth.getCurrentUser();
        String uid=mUser.getUid();

        mIncomeDatabase= FirebaseDatabase.getInstance("https://moneytracker2-6a1af-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("IncomeData").child(uid);
        mExpenseDatabase=FirebaseDatabase.getInstance("https://moneytracker2-6a1af-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("ExpenseDatabase").child(uid);

        // For Generate Button
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String finalInput = input.getText().toString();
                if(finalInput.length() > 0 && finalInput !=null){
                    try {
                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        BitMatrix bitMatrix = multiFormatWriter.encode(finalInput,BarcodeFormat.QR_CODE, 700, 700);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                        display_qr.setImageBitmap(bitmap);
                    }catch(WriterException e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Please Enter Some Value",Toast.LENGTH_SHORT).show();
                }
            }
        });

        // For Scan Button
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(QR_activity.this);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                intentIntegrator.setCameraId(0);
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.setPrompt("scanning");
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setBarcodeImageEnabled(true);
                intentIntegrator.initiateScan();

            }
        });
    }

    //For Result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null && result.getContents() != null) {

            new AlertDialog.Builder(QR_activity.this)
                    .setTitle("Scan Result")
                    .setMessage(result.getContents())
                    .setPositiveButton("Income", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String type= "QR";
                            String ammount=result.getContents();
                            String note="QR";
                            String id=mIncomeDatabase.push().getKey();
                            String mDate = DateFormat.getDateInstance().format(new Date());
                            int ourammontint=Integer.parseInt(ammount);
                            Data data=new Data(ourammontint,type,note,id,mDate);
                            mIncomeDatabase.child(id).setValue(data);

                        }
                    }).setNegativeButton("Expense", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String type= "QR";
                    String ammount=result.getContents();
                    String note="QR";
                    String id=mExpenseDatabase.push().getKey();
                    String mDate = DateFormat.getDateInstance().format(new Date());
                    int ourammontint=Integer.parseInt(ammount);
                    Data data=new Data(ourammontint,type,note,id,mDate);
                    mExpenseDatabase.child(id).setValue(data);
                }
            }).create().show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

}
